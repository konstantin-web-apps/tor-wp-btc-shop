#!/bin/bash

if [ "$1" == "generate" ]
then
    if [ -f /var/www/tor/*secret_key ]
    then
        echo '[-] You already have an private key, delete it if you want to generate a new key'
        exit -1
    fi
    if [ -z "$2" ]
    then
        echo '[-] You dont provided any mask, please inform an mask to generate your address'
        exit -1
    else
        echo '[+] Generating the address with mask: '$2
        if [ -d /tmp/keys ]
        then
            rm /tmp/keys/ -r
        else
            mkdir /tmp/keys
        fi
        mkp224o $2 -n 1 -d /tmp/keys &> /dev/null
        echo '[+] Found '$(cat /tmp/keys/*.onion/hostname)
        cp /tmp/keys/*.onion/*secret_key /var/www/tor/
        cp /tmp/keys/*.onion/hostname /var/www/tor/
    fi
fi

if [ "$1" == "serve" ]
then
    if [ ! -f /var/www/tor/*secret_key ]
    then
        echo '[-] Please run this container with generate argument to initialize your web page'
        exit -1
    fi
    echo '[+] Initializing local clock'
    ntpdate -B -q 0.debian.pool.ntp.org
    echo '[+] Starting tor'
    chown -R hidden:hidden /var/www/tor
    chmod -R 700 /var/www/tor
    sudo -u hidden tor -f /etc/tor/torrc &
    echo '[+] Starting nginx'
    chown -R www-data:www-data /var/www/html/
    chmod -R 755 /var/www/html/
    nginx &
    
    # Monitor logs
    sleep infinity
fi
